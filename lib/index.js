const express = require('express');
const path = require('path');

module.exports = class LoggerUI {
    /**
     * Construct the LoggerUI
     *
     * @param {string[]} files
     * @param {number} rowsPerPage
     */
    constructor(files = [], rowsPerPage = 30) {
        /**
         * @type {Express}
         */
        this.app = express();
        /**
         * A list with absolute paths to the log files that should be visible
         *
         * @type {string[]}
         */
        this.app.locals.files = files;
        /**
         * The amount of rows that should be displayed per file
         *
         * @type {number}
         */
        this.app.locals.rowsPerPage = rowsPerPage;
    }

    /**
     * Attach our UI as a sub application
     *
     * @returns {Express}
     */
    attach() {
        this.app.set('view engine', 'ejs');
        this.app.set('views', `${__dirname}/views`);

        this.app.use('/public', express.static(path.join(__dirname, '/public')));

        this.app.get('/', require('./routes/index'));
        this.app.get('/:log_file', require('./routes/index'));
        this.app.get('/:log_file/:page', require('./routes/index'));

        return this.app;
    }

    /**
     * Moves our application to the front of the stack so any logging
     * from the application for requests to this UI are not logged.
     *
     * @param {Express} app
     * @param {string} layerName
     */
    asFirstMiddleware(app, layerName = 'expressInit') {
        // eslint-disable-next-line no-underscore-dangle
        if (typeof app._router === 'undefined') {
            throw Error('Unfortunately moving middlewares no longer works!');
        }

        // eslint-disable-next-line no-underscore-dangle
        const layerIndex = app._router.stack.findIndex((layer) => layer.name === layerName);

        // eslint-disable-next-line no-underscore-dangle
        app._router.stack.splice(
            layerIndex + 1,
            0,
            // eslint-disable-next-line no-underscore-dangle
            app._router.stack.splice(app._router.stack.length - 1, 1)[0],
        );
    }
};
