const fs = require('fs');
const path = require('path');
const readline = require('readline');

module.exports = class LogReader {
    /**
     *
     * @param {string[]} files
     * @param {string|null} file
     * @param {number} rowsPerPage
     */
    constructor(files, file = null, rowsPerPage) {
        /**
         * All files visible in the UI
         *
         * @type {string[]}
         */
        this.files = files;
        /**
         * Absolute path to our current file
         *
         * @type {string}
         */
        this.file = file || files[0];
        /**
         * Basename of our current file
         *
         * @type {string}
         */
        this.currentFile = path.basename(this.file);
        /**
         * Rows to show per page
         *
         * @type {number}
         */
        this.rowsPerPage = rowsPerPage;
        /**
         * Total lines in the current file
         *
         * @type {number}
         */
        this.totalLines = 0;
    }

    /**
     * Initialize a readline
     *
     * @returns {ReadLine}
     */
    init() {
        return readline.createInterface({
            input: fs.createReadStream(this.file),
        });
    }

    /**
     * Read the given range of lines
     *
     * @param {number} total
     * @param {number} pages
     * @param {number} startLine
     * @param {number} endLine
     * @returns {Promise<string{}>}
     */
    read(total, pages, startLine, endLine) {
        return new Promise((resolve, reject) => {
            try {
                let currentLine = 0;
                const output = [];
                const readInterface = this.init();

                return readInterface
                    .on('line', (line) => {
                        // Within requested range, save lines for our output
                        if (
                            currentLine >= startLine
                            && currentLine < endLine
                            && line.length > 1
                        ) {
                            output.push(JSON.parse(line));
                        }

                        // Stop reading
                        if (currentLine >= endLine) {
                            readInterface.close();
                        }

                        currentLine += 1;
                    })
                    .on('close', () => resolve({
                        total,
                        pages,
                        startLine,
                        rows: output.reverse(),
                        endLine: currentLine > endLine ? endLine : currentLine,
                        currentFile: this.currentFile,
                        filesList: this.files.map((file) => path.basename(file)),
                    }));
            } catch (err) {
                return reject(err);
            }
        });
    }

    /**
     * Read the next X rows
     *
     * @param {number} page
     * @returns {Promise<string{}>}
     */
    next(page = 1) {
        return this.total()
            .then((total) => {
                if (this.rowsPerPage === 0) {
                    return this.read(total, 0, 0, total);
                }

                const startLine = total > this.rowsPerPage ? total - (page * this.rowsPerPage) : 0;
                const endLine = startLine + this.rowsPerPage;

                return this.pages()
                    .then((pages) => this.read(total, pages, startLine, endLine));
            });
    }

    /**
     * Return the number of pages we need to view the entire file
     *
     * @returns {Promise<number>}
     */
    pages() {
        return this.total()
            .then((total) => new Promise((resolve) => {
                resolve(Math.ceil(total / this.rowsPerPage));
            }));
    }

    /**
     * Return the total amount of lines in the file
     *
     * @returns {Promise<number>}
     */
    total() {
        return new Promise((resolve, reject) => {
            if (this.totalLines > 0) {
                return resolve(this.totalLines);
            }

            try {
                return this.init()
                    .on('line', () => {
                        this.totalLines += 1;
                    })
                    .on('close', () => resolve(this.totalLines));
            } catch (err) {
                return reject(err);
            }
        });
    }
};
