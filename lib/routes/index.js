const LogReader = require('../reader');

module.exports = async (req, res) => {
    const basePath = req.proxyUrl || req.baseUrl;
    const page = parseInt(req.params.page) || 1;

    const reader = new LogReader(
        req.app.locals.files,
        req.params.log_file,
        req.params.page === 'all' ? 0 : req.app.locals.rowsPerPage,
    );

    reader
        .next(page)
        .then((result) => {
            res.render('index', {
                basePath,
                page,
                logFile: result.currentFile,
                files: result.filesList,
                showAll: req.params.page === 'all',
                pages: result.pages,
                total: result.total,
                rows: result.rows,
                start: result.startLine,
                end: result.endLine,
            });
        })
        .catch((err) => {
            console.error(err);
        });
};
