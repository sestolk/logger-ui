# Logger UI
A log viewer for in the browser compatible with logs created with [Winston](https://github.com/winstonjs/winston).

The logger UI uses external CDN's to supply the assets. So for the best experience using the Logger UI an internet connection is advised!

> _If you find out it also works with other loggers unlike Winston, please let me know and I will update this readme!_

## Requirements
- ExpressJS >=4
- Winston-like logfiles

## Installation
Pull in the package using the command below:
`npm install logger-ui`

```js
const LoggerUI = require('logger-ui');
// ...
const files = [
    '/path/to/logfile'
];
const logger = new LoggerUI(files);

app.use('/logger', logger.attach());
```
You should now be able to go to `/logger` in your browser and see the UI.


## Configuration
### Rows per page
You can configure the amount of rows that is visible per page (default: 30). Just supply the second argument to the LoggerUI constructor like so:
```js
// ...
const logger = new LoggerUI(files, 50); // 50 rows per page
// ...
```

### Middlewares
You might run into the issue when you have a Middleware attached to your application that logs any incoming requests to your application. That it now also logs any request to the logger. This will very quickly clutter your logs with these requests.

To circumvent this issue you can add the following line just below the `use()` statement.
```js
// ...
logger.asFirstMiddleware(app);
```
Make sure there are no other `use()` statements before this line except the one from Logger UI.

#### Credits
- [Material Design for Bootstrap](https://fezvrasta.github.io/bootstrap-material-design/)
